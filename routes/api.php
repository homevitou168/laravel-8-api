<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', [\App\Http\Controllers\API\RegisterController::class, 'register']);
Route::post('login', [\App\Http\Controllers\API\RegisterController::class, 'login']);

Route::middleware('auth:api')->group( function () {
    Route::resource('products', \App\Http\Controllers\API\ProductController::class);
    Route::get('collectionHelper', function () {

        $collections = collect([
            [
                'id'=>1,
                'name'=>'sovannary'
            ],
            [
                'id'=>2,
                'name'=>'seyha'
            ],
            [
                'id'=>3,
                'name'=>'kongkea'
            ],

        ]);

//        return \Illuminate\Http\JsonResponse::create( \App\Helpers\General\CollectionHelper::paginate($collections, '10'));

        return new \Illuminate\Http\JsonResponse(\App\Helpers\General\CollectionHelper::paginate($collections, '10'));
    });


});
